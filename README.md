Connect4Zero
============

A Connect Four mobile game that uses an AI based on AlphaZero as an opponent.

For full details see [Connect4Zero.pdf](Connect4Zero.pdf)

Training Loss
-------------

![Model training loss plot](img/loss.png)

First move probability as training progresses
---------------------------------------------

![First move probability as training progresses](img/first_move_probs.png)

App Screenshot
--------------

![Application screenshot](img/screenshot.png)

Model
-----

![Model structure](img/model.png)
