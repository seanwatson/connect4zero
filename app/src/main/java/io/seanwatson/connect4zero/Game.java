package io.seanwatson.connect4zero;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.os.SystemClock;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tensorflow.lite.Interpreter;

/**
 * Created by sean on 31/03/18.
 */

public class Game {

    public class MoveResult {
        int row;
        int col;

        int winner;

        float winConfidence;
        int[] lastMoveSuggestions;
        float[] lastMoveConfidences;
        int[] moveSuggestions;
        float[] moveConfidences;
        long predictionLatencyMs;
    }

    public static final int NUM_ROWS = 6;
    public static final int NUM_COLS = 7;
    public static final int EMPTY = 0;
    public static final int PLAYER_ONE = 1;
    public static final int PLAYER_TWO = 2;
    public static final int TIE = 3;
    public static final int NUM_SUGGESTIONS = 3;

    private static final String MODEL_PATH = "graph.lite";

    private int board[][];
    private Interpreter tflite;
    private int[][] winners = {
            {0, 1, 2, 3},
            {1, 2, 3, 4},
            {2, 3, 4, 5},
            {3, 4, 5, 6},
            {7, 8, 9, 10},
            {8, 9, 10, 11},
            {9, 10, 11, 12},
            {10, 11, 12, 13},
            {14, 15, 16, 17},
            {15, 16, 17, 18},
            {16, 17, 18, 19},
            {17, 18, 19, 20},
            {21, 22, 23, 24},
            {22, 23, 24, 25},
            {23, 24, 25, 26},
            {24, 25, 26, 27},
            {28, 29, 30, 31},
            {29, 30, 31, 32},
            {30, 31, 32, 33},
            {31, 32, 33, 34},
            {35, 36, 37, 38},
            {36, 37, 38, 39},
            {37, 38, 39, 40},
            {38, 39, 40, 41},

            {0, 7, 14, 21},
            {7, 14, 21, 28},
            {14, 21, 28, 35},
            {1, 8, 15, 22},
            {8, 15, 22, 29},
            {15, 22, 29, 36},
            {2, 9, 16, 23},
            {9, 16, 23, 30},
            {16, 23, 30, 37},
            {3, 10, 17, 24},
            {10, 17, 24, 31},
            {17, 24, 31, 38},
            {4, 11, 18, 25},
            {11, 18, 25, 32},
            {18, 25, 32, 39},
            {5, 12, 19, 26},
            {12, 19, 26, 33},
            {19, 26, 33, 40},
            {6, 13, 20, 27},
            {13, 20, 27, 34},
            {20, 27, 34, 41},

            {3, 9, 15, 21},
            {4, 10, 16, 22},
            {10, 16, 22, 28},
            {5, 11, 17, 23},
            {11, 17, 23, 29},
            {17, 23, 29, 35},
            {6, 12, 18, 24},
            {12, 18, 24, 30},
            {18, 24, 30, 36},
            {13, 19, 25, 31},
            {19, 25, 31, 37},
            {20, 26, 32, 38},

            {3, 11, 19, 27},
            {2, 10, 18, 26},
            {10, 18, 26, 34},
            {1, 9, 17, 25},
            {9, 17, 25, 33},
            {17, 25, 33, 41},
            {0, 8, 16, 24},
            {8, 16, 24, 32},
            {16, 24, 32, 40},
            {7, 15, 23, 31},
            {15, 23, 31, 39},
            {14, 22, 30, 38}
    };


    public Game(Activity activity) throws IOException {
        board = new int[NUM_ROWS][NUM_COLS];
        tflite = new Interpreter(loadModelFile(activity));

        reset();
    }

    public void reset() {
        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                board[i][j] = EMPTY;
            }
        }
    }

    public MoveResult move(int row, int col) {
        board[row][col] = PLAYER_ONE;

        MoveResult result = new MoveResult();
        result.winner = checkForWinner();
        if (result.winner != EMPTY) {
            return result;
        }

        float[][] value = new float[1][1];
        float[][] policy = new float[1][42];
        float[][][][] input = getModelInput(PLAYER_TWO);
        Map<Integer, Object> map_of_indices_to_outputs = new HashMap<>();
        Object[] input_obj = {input};
        map_of_indices_to_outputs.put(0, value);
        map_of_indices_to_outputs.put(1, policy);
        long startTime = SystemClock.uptimeMillis();
        tflite.runForMultipleInputsOutputs(input_obj, map_of_indices_to_outputs);
        long endTime = SystemClock.uptimeMillis();
        result.predictionLatencyMs = endTime - startTime;
        getMoves(result, policy);
        int move = result.moveSuggestions[0];
        int mustMove = checkForMustMove();
        if (mustMove >= 0) {
            move = mustMove;
        }

        result.row = move / NUM_COLS;
        result.col = move % NUM_COLS;
        result.winConfidence = (value[0][0] + 1) / 2 * 100;
        result.lastMoveConfidences = result.moveConfidences;
        result.lastMoveSuggestions = result.moveSuggestions;

        board[result.row][result.col] = PLAYER_TWO;
        result.winner = checkForWinner();
        if (result.winner != EMPTY) {
            return result;
        }

        MoveResult result2 = new MoveResult();
        value = new float[1][1];
        policy = new float[1][42];
        input = getModelInput(PLAYER_ONE);
        map_of_indices_to_outputs = new HashMap<>();
        input_obj[0] = input;
        map_of_indices_to_outputs.put(0, value);
        map_of_indices_to_outputs.put(1, policy);
        tflite.runForMultipleInputsOutputs(input_obj, map_of_indices_to_outputs);
        getMoves(result2, policy);
        result.moveSuggestions = result2.moveSuggestions;
        result.moveConfidences = result2.moveConfidences;

        return result;
    }

    public int getRow(int col) {
        for (int i = NUM_ROWS - 1; i >= 0; i--) {
            if (board[i][col] == EMPTY) {
                return i;
            }
        }
        return -1;
    }

    private int checkForWinner() {
        int result = TIE;
        for (int i = 0; i < NUM_COLS; i++) {
            if (board[0][i] == EMPTY) {
                result = EMPTY;
                break;
            }
        }
        for (int[] winner : winners) {
            int row1 = winner[0] / NUM_COLS;
            int col1 = winner[0] % NUM_COLS;
            int row2 = winner[1] / NUM_COLS;
            int col2 = winner[1] % NUM_COLS;
            int row3 = winner[2] / NUM_COLS;
            int col3 = winner[2] % NUM_COLS;
            int row4 = winner[3] / NUM_COLS;
            int col4 = winner[3] % NUM_COLS;

            if (board[row1][col1] != EMPTY &&
                    board[row1][col1] == board[row2][col2] &&
                    board[row1][col1] == board[row3][col3] &&
                    board[row1][col1] == board[row4][col4]) {
                return board[row1][col1];
            }
        }
        return result;
    }

    private int checkForMustMove() {
        for (int[] winner : winners) {
            int row1 = winner[0] / NUM_COLS;
            int col1 = winner[0] % NUM_COLS;
            int row2 = winner[1] / NUM_COLS;
            int col2 = winner[1] % NUM_COLS;
            int row3 = winner[2] / NUM_COLS;
            int col3 = winner[2] % NUM_COLS;
            int row4 = winner[3] / NUM_COLS;
            int col4 = winner[3] % NUM_COLS;

            if (board[row1][col1] == PLAYER_ONE
                || board[row2][col2] == PLAYER_ONE
                || board[row3][col3] == PLAYER_ONE
                || board[row4][col4] == PLAYER_ONE) {
                continue;
            }
            int empty = -1;
            if (board[row1][col1] == EMPTY) {
                empty = 0;
            }
            if (board[row2][col2] == EMPTY) {
                if (empty >= 0) { continue; } else { empty = 1; }
            }
            if (board[row3][col3] == EMPTY) {
                if (empty >= 0) { continue; } else { empty = 2; }
            }
            if (board[row4][col4] == EMPTY) {
                if (empty >= 0) { continue; } else { empty = 3; }
            }
            if (empty >= 0) {
                int r = winner[empty] / NUM_COLS;
                int c = winner[empty] % NUM_COLS;
                if (r == getRow(c)) {
                    return winner[empty];
                }
            }
        }

        for (int[] winner : winners) {
            int row1 = winner[0] / NUM_COLS;
            int col1 = winner[0] % NUM_COLS;
            int row2 = winner[1] / NUM_COLS;
            int col2 = winner[1] % NUM_COLS;
            int row3 = winner[2] / NUM_COLS;
            int col3 = winner[2] % NUM_COLS;
            int row4 = winner[3] / NUM_COLS;
            int col4 = winner[3] % NUM_COLS;

            if (board[row1][col1] == PLAYER_TWO
                    || board[row2][col2] == PLAYER_TWO
                    || board[row3][col3] == PLAYER_TWO
                    || board[row4][col4] == PLAYER_TWO) {
                continue;
            }
            int empty = -1;
            if (board[row1][col1] == EMPTY) {
                empty = 0;
            }
            if (board[row2][col2] == EMPTY) {
                if (empty >= 0) { continue; } else { empty = 1; }
            }
            if (board[row3][col3] == EMPTY) {
                if (empty >= 0) { continue; } else { empty = 2; }
            }
            if (board[row4][col4] == EMPTY) {
                if (empty >= 0) { continue; } else { empty = 3; }
            }
            if (empty >= 0) {
                int r = winner[empty] / NUM_COLS;
                int c = winner[empty] % NUM_COLS;
                if (r == getRow(c)) {
                    return winner[empty];
                }
            }
        }
        return -1;
    }

    private float[][][][] getModelInput(int player) {
        float[][][][] input = new float[1][6][7][2];
        int player_one_dim = 0;
        int player_two_dim = 1;
        if (player == PLAYER_TWO) {
            player_one_dim = 1;
            player_two_dim = 0;
        }
        for (int i = 0; i < NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                if (board[i][j] == PLAYER_ONE) {
                    input[0][i][j][player_one_dim] = 1;
                } else if (board[i][j] == PLAYER_TWO) {
                    input[0][i][j][player_two_dim] = 1;
                }
            }
        }
        return input;
    }

    private void getMoves(MoveResult result, float[][] policy) {
        for (int i = 0; i < policy[0].length; i++) {
            int row = i / NUM_COLS;
            int col = i % NUM_COLS;
            if (getRow(col) == row) {
                continue;
            }
            policy[0][i] = -100;
        }
        double[] policy_exp = new double[policy[0].length];
        double policy_exp_sum = 0;
        for (int i = 0; i < policy[0].length; i++) {
            policy_exp[i] = Math.exp(policy[0][i]);
            policy_exp_sum += policy_exp[i];
        }
        for (int i = 0; i < policy[0].length; i++) {
            policy_exp[i] = policy_exp[i] / policy_exp_sum;
        }
        result.moveSuggestions = new int[NUM_SUGGESTIONS];
        result.moveConfidences = new float[NUM_SUGGESTIONS];

        for (int i = 1; i < policy[0].length; i++) {
            if (policy_exp[i] > policy_exp[result.moveSuggestions[0]]) {
                for (int j = NUM_SUGGESTIONS - 1; j > 0; j--) {
                    result.moveSuggestions[j] = result.moveSuggestions[j - 1];
                }
                result.moveSuggestions[0] = i;
                continue;
            }
            if (policy_exp[i] > policy_exp[result.moveSuggestions[1]]) {
                for (int j = NUM_SUGGESTIONS - 1; j > 1; j--) {
                    result.moveSuggestions[j] = result.moveSuggestions[j - 1];
                }
                result.moveSuggestions[1] = i;
                continue;
            }
            if (policy_exp[i] > policy_exp[result.moveSuggestions[2]]) {
                for (int j = NUM_SUGGESTIONS - 1; j > 2; j--) {
                    result.moveSuggestions[j] = result.moveSuggestions[j - 1];
                }
                result.moveSuggestions[2] = i;
                continue;
            }
        }
        for (int i = 0; i < NUM_SUGGESTIONS; i++) {
            result.moveConfidences[i] = (float) policy_exp[result.moveSuggestions[i]];
        }
    }

    private MappedByteBuffer loadModelFile(Activity activity) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_PATH);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }
}
