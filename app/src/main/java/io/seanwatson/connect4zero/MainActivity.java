package io.seanwatson.connect4zero;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import static io.seanwatson.connect4zero.Game.NUM_COLS;

public class MainActivity extends AppCompatActivity {

    private static final int[][] positionIds = {
            {R.id.pos_1, R.id.pos_2, R.id.pos_3, R.id.pos_4, R.id.pos_5, R.id.pos_6, R.id.pos_7},
            {R.id.pos_8, R.id.pos_9, R.id.pos_10, R.id.pos_11, R.id.pos_12, R.id.pos_13, R.id.pos_14},
            {R.id.pos_15, R.id.pos_16, R.id.pos_17, R.id.pos_18, R.id.pos_19, R.id.pos_20, R.id.pos_21},
            {R.id.pos_22, R.id.pos_23, R.id.pos_24, R.id.pos_25, R.id.pos_26, R.id.pos_27, R.id.pos_28},
            {R.id.pos_29, R.id.pos_30, R.id.pos_31, R.id.pos_32, R.id.pos_33, R.id.pos_34, R.id.pos_35},
            {R.id.pos_36, R.id.pos_37, R.id.pos_38, R.id.pos_39, R.id.pos_40, R.id.pos_41, R.id.pos_42}
    };

    private ImageView[][] positions;
    private Button[] buttons;
    private TextView info;
    private TextView suggestions;
    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button resetButton = findViewById(R.id.reset);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
            }
        });

        Button columnButton1 = findViewById(R.id.column_1);
        columnButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                move(0);
            }
        });
        Button columnButton2 = findViewById(R.id.column_2);
        columnButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                move(1);
            }
        });
        Button columnButton3 = findViewById(R.id.column_3);
        columnButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                move(2);
            }
        });
        Button columnButton4 = findViewById(R.id.column_4);
        columnButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                move(3);
            }
        });
        Button columnButton5 = findViewById(R.id.column_5);
        columnButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                move(4);
            }
        });
        Button columnButton6 = findViewById(R.id.column_6);
        columnButton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                move(5);
            }
        });
        Button columnButton7 = findViewById(R.id.column_7);
        columnButton7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                move(6);
            }
        });

        Button hints = findViewById(R.id.hints);
        hints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button hints = findViewById(R.id.hints);
                TextView suggestions = findViewById(R.id.suggestions);
                if (suggestions.getVisibility() == View.VISIBLE) {
                    hints.setText("Show Move Suggestions");
                    suggestions.setVisibility(View.INVISIBLE);
                } else if (suggestions.getVisibility() == View.INVISIBLE) {
                    hints.setText("Hide Move Suggestions");
                    suggestions.setVisibility(View.VISIBLE);
                }

            }
        });

        positions = new ImageView[Game.NUM_ROWS][NUM_COLS];
        for (int i = 0; i < Game.NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                positions[i][j] = findViewById(positionIds[i][j]);
            }
        }

        buttons = new Button[NUM_COLS];
        buttons[0] = columnButton1;
        buttons[1] = columnButton2;
        buttons[2] = columnButton3;
        buttons[3] = columnButton4;
        buttons[4] = columnButton5;
        buttons[5] = columnButton6;
        buttons[6] = columnButton7;
        info = findViewById(R.id.info);
        suggestions = findViewById(R.id.suggestions);

        try {
            game = new Game(this);
        } catch (Exception e) {

        }

        reset();
        updateInfo(Game.EMPTY, 0, 0, null, null, null, null);
    }

    private void reset() {
        for (int i = 0; i < Game.NUM_ROWS; i++) {
            for (int j = 0; j < NUM_COLS; j++) {
                positions[i][j].setImageResource(R.drawable.empty);
            }
        }
        game.reset();
        enableButtons();
        updateInfo(Game.EMPTY, 0, 0, null, null, null, null);
    }

    private void move(int col) {
        int row = game.getRow(col);
        if (row == 0) {
            disableButton(col);
        }
        positions[row][col].setImageResource(R.drawable.red);
        Game.MoveResult result = game.move(row, col);
        if (result.winner == Game.PLAYER_ONE) {
            disableButtons();
        } else if (result.winner == Game.PLAYER_TWO) {
            disableButtons();
            positions[result.row][result.col].setImageResource(R.drawable.yellow);
        } else if (result.winner == Game.TIE) {
            disableButtons();
            positions[result.row][result.col].setImageResource(R.drawable.yellow);
        } else {
            positions[result.row][result.col].setImageResource(R.drawable.yellow);
        }
        if (result.row == 0) {
            disableButton(result.col);
        }


        updateInfo(
                result.winner, result.winConfidence, result.predictionLatencyMs,
                result.lastMoveSuggestions, result.lastMoveConfidences,
                result.moveSuggestions, result.moveConfidences);
    }

    private void updateInfo(
            int winner, float winConfidence, long predictionLatency,
            int[] lastMoveSuggestions, float[] lastMoveConfidences,
            int[] moveSuggestions, float[] moveConfidences) {
        String text = "";
        if (winner == Game.PLAYER_ONE) {
            text += "You WIN!\n\n";
        } else if (winner == Game.PLAYER_TWO) {
            text += "CPU WINS!\n\n";
        } else if (winner == Game.TIE) {
            text += "TIE!\n\n";
        } else {
            text += "Select your next move...\n\n";
        }
        text += "CPU win/tie confidence: ";
        if (winConfidence > 0) {
            text += Float.toString(winConfidence) + "%";
        }
        text += "\n";
        text += "CPU last move confidence: ";
        if (lastMoveConfidences != null) {
            for (int i = 0; i < Game.NUM_SUGGESTIONS; i++) {
                if (lastMoveConfidences[i] > 0.02) {
                    text += Integer.toString((lastMoveSuggestions[i] % Game.NUM_COLS) + 1);
                    text += " (" + Integer.toString(Math.round(lastMoveConfidences[i] * 100)) + "%) ";
                }
            }
        }
        text += "\n";
        text += "Prediction latency: ";
        if (predictionLatency > 0) {
            text += Long.toString(predictionLatency) + "ms";
        }
        text += "\n";
        String suggestion_text = "Move Suggestions: ";
        if (moveSuggestions != null) {
            for (int i = 0; i < Game.NUM_SUGGESTIONS; i++) {
                if (moveConfidences[i] > 0.02) {
                    suggestion_text += Integer.toString((moveSuggestions[i] % Game.NUM_COLS) + 1);
                    suggestion_text += " (" + Integer.toString(Math.round(moveConfidences[i] * 100)) + "%) ";
                }
            }
        }
        info.setText(text);
        suggestions.setText(suggestion_text);
    }

    private void enableButtons() {
        for (Button button : buttons) {
            button.setEnabled(true);
        }
    }

    private void disableButtons() {
        for (Button button : buttons) {
            button.setEnabled(false);
        }
    }

    private void disableButton(int button) {
        buttons[button].setEnabled(false);
    }
}
